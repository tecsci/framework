# Framework TECSCI   Technology for Science 

## How to use example
## Example folder contents

```
├── CMakeLists.txt
|
|-- components
|
|── main
|   ├── CMakeLists.txt
|   ├── component.mk           Component make file
|   └── hello_world_main.c
|── Makefile                   Makefile used by legacy GNU Make
|── README.md                  This is the file you are currently reading
```
## Troubleshooting

* Program upload failure

    * Hardware connection is not correct: run `idf.py -p PORT monitor`, and reboot your board to see if there are any output logs.
    * The baud rate for downloading is too high: lower your baud rate in the `menuconfig` menu, and try again.

## Technical support and feedback

Please use the following feedback channels:

info@tecsci.com.ar

We will get back to you as soon as possible.
