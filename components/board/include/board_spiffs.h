/* 
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_spiffs.h
* @date		30 sep. 2021
* @author	
*		-Lucas Mancini   <xx>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef COMPONENTS_BOARD_INCLUDE_BOARD_SPIFFS_H_
#define COMPONENTS_BOARD_INCLUDE_BOARD_SPIFFS_H_
/********************** inclusions *******************************************/

#include "esp_spiffs.h"
/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void board_spiffs_config(const char * path);
bool board_spiffs_mount();
void board_spiffs_unmount();
bool board_spiffs_is_mounted();
FILE* board_spiffs_open(const char * path, const char* mode);
void board_spiffs_close(FILE* f);
bool board_spiffs_delete_file(const char * path);
bool board_spiffs_rename_file(const char * path_old,const char * path_new);

bool board_spiffs_read_line(FILE* f, char * buff, uint32_t size);
bool board_spiffs_read_n_line(FILE* f, char * buff, uint32_t size, uint32_t line_n);
void board_spiffs_write(FILE* f, const char * message);


void board_spiffs_init();
void board_spiffs_deinit();
void board_spiffs_loop();

#endif /* COMPONENTS_BOARD_INCLUDE_BOARD_SPIFFS_H_ */
/** @}Final Doxygen */
