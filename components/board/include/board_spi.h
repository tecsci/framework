/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_spi.h
* @date		14 jun. 2021
* @author	
*		-martin
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

#ifndef COMPONENTS_BOARD_INCLUDE_BOARD_SPI_H_
#define COMPONENTS_BOARD_INCLUDE_BOARD_SPI_H_
/********************** inclusions *******************************************/

#include "stdint.h"
#include "stddef.h"

/********************** macros ***********************************************/
typedef enum{

	BOARD_SPI_0=0, //console
	BOARD_SPI_1=1,
	BOARD_SPI_2=2,
	BOARD_SPI_3=3,
	BOARD_SPI_MAX

}board_spi_id_t;
/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/
void board_spi_read(void);
void board_spi_write_read(board_spi_id_t id, uint8_t *tx, uint8_t *rx, size_t length);


void board_spi_init(void);
void board_spi_deinit(void);
void board_spi_loop(void);


#endif /* COMPONENTS_BOARD_INCLUDE_BOARD_SPI_H_ */
/** @}Final Doxygen */
