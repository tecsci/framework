/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_delay.c
* @date		23 sep. 2021
* @author	
*		-Martin Abel Gambarotta   <magambarotta@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "board_delay.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"



/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void board_delay_micros(uint32_t micros){
	vTaskDelay((micros/portTICK_PERIOD_MS)/1000);
}
void board_delay_milis(uint32_t milis){
	vTaskDelay((milis/portTICK_PERIOD_MS));
}
void board_delay_seconds(uint32_t seconds){
	vTaskDelay((seconds/portTICK_PERIOD_MS)*1000);
}


void board_delay_init(){}
void board_delay_deinit(){}
void board_delay_loop(){}

/********************** end of file ******************************************/

/** @}Final Doxygen */
