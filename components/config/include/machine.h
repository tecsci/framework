/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		machine.h
* @date		15 jun. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef COMPONENTS_CONFIG_INCLUDE_MACHINE_H_
#define COMPONENTS_CONFIG_INCLUDE_MACHINE_H_
/********************** inclusions *******************************************/

/********************** macros ***********************************************/

// Constante de diferencia de velocidad debido a que no esta el clk de 16MHZ externo en nuestro diseño
//#define K_VELOCIDAD  1.34
#define K_VELOCIDAD  (1)

// Constante de dezplazamiento lineal   , expresada en mm/micropasos   ->   una vuelta   1 paso  ==   0.00007851  mm     -->    (una vuelta) 51200   ==   4.02 mm
#define  K_DEZPLAZAMIENTO_LINEAL   (0.00007851)   //  78.51E-6



#define MACHINE_CONTROL_MECANICAL_UPPER_LIMIT 		(0x0000F8C6)
#define MACHINE_CONTROL_MECANICAL_LOWER_LIMIT			(0x003E3186)



/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

#endif /* COMPONENTS_CONFIG_INCLUDE_MACHINE_H_ */
/** @}Final Doxygen */

