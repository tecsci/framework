/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		xsx.c
* @date		1 oct. 2021
* @author
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
*
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/

#include "api_program_manager.h"
#include "board_spiffs.h"
#include "rsc_json_lib.h"
#include "esp_log.h"


/********************** macros and definitions *******************************/



/********************** internal data declaration ****************************/

static struct{


	char aux_data_buff[API_PROGRAM_DATA_BUFF_SZ];
	char aux_filename_buff[API_PROGRAM_FILENAME_BUFF_SZ];

}self_;

static api_program_manager_pf_config_t* self_config_;

static const char *TAG = "api_program_manager";

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static bool api_program_manager_delete_(uint8_t index){

	if (API_PROGRAM_MAX_PROGRAM_INDEX < index || (index == 0)){
		return false;
	}

	char aux_name_itoa[4]={0};

    strcpy(self_.aux_filename_buff, "");
    strcat(self_.aux_filename_buff, API_PROGRAM_FILENAME_BASE);
    itoa(index, aux_name_itoa, 10);
    strcat(self_.aux_filename_buff, aux_name_itoa);
    strcat(self_.aux_filename_buff, ".txt");

    return self_config_->user_fs_deleting_func(self_.aux_filename_buff);

}


#if 0
bool user_serialize_func(api_program_manager_program_t * program, char * buffer, uint32_t buffer_size){
	return false;
}

bool user_deserialize_func(api_program_manager_program_t * program, char * buffer, uint32_t buffer_size){
	return false;
}


bool user_fs_writing_func(const char * path , char * buffer, uint32_t buffer_size){
	return false;
}

bool user_fs_reading_func(const char * path , char * buffer, uint32_t buffer_size){
	return false;
}

bool user_fs_deleting_func(const char * path ){
	return false;
}
#endif
/********************** external functions definition ************************/


bool api_program_manager_write_in_empty(api_program_manager_program_t * p_program){

	if(NULL == p_program){
		return false;
	}

	uint32_t next_empty= 1 + api_program_manager_get_n_programs_in_mem();

	return api_program_manager_write(p_program, next_empty);
}

bool api_program_manager_write(api_program_manager_program_t * p_program, uint8_t index){

	if (API_PROGRAM_MAX_PROGRAM_INDEX < index || (index == 0)){
        return false;
    }

	if(NULL == p_program){
		return false;
	}

    char aux_name_itoa[4]={0};

	self_config_->user_serialize_func(p_program, self_.aux_data_buff, API_PROGRAM_DATA_BUFF_SZ);
	ESP_LOGI(TAG,"data serialized: %s",self_.aux_data_buff);

    strcpy( self_.aux_filename_buff, "");
    strcat( self_.aux_filename_buff, API_PROGRAM_FILENAME_BASE);
    itoa(index, aux_name_itoa, 10);
    strcat(self_.aux_filename_buff, aux_name_itoa);
    strcat(self_.aux_filename_buff, ".txt");

    return self_config_->user_fs_writing_func(self_.aux_filename_buff, self_.aux_data_buff, API_PROGRAM_DATA_BUFF_SZ);

}

bool  api_program_manager_read(api_program_manager_program_t * program, uint8_t index){

	if (API_PROGRAM_MAX_PROGRAM_INDEX < index || (index == 0)){
        return false;
    }

    char aux_name_itoa[4]={0};

    strcpy(self_.aux_filename_buff, "");
    strcat(self_.aux_filename_buff, API_PROGRAM_FILENAME_BASE);
    itoa(index, aux_name_itoa, 10);
    strcat(self_.aux_filename_buff, aux_name_itoa);
    strcat(self_.aux_filename_buff, ".txt");

    if (self_config_->user_fs_reading_func(self_.aux_filename_buff, self_.aux_data_buff, API_PROGRAM_DATA_BUFF_SZ) == true)
    {
    	self_config_->user_deserialize_func(program, self_.aux_data_buff, API_PROGRAM_DATA_BUFF_SZ);
        return true;
    }

        return false;
}



bool api_program_manager_delete_and_leftshift(uint8_t index){

	if (API_PROGRAM_MAX_PROGRAM_INDEX < index || (index == 0) ){
		return false;
	}

	api_program_manager_program_t aux_program;
	uint32_t programs_in_mem = api_program_manager_get_n_programs_in_mem()-1;

	if(false == api_program_manager_delete_(index)){
		return false;
	}

	char old_filename_buff[API_PROGRAM_FILENAME_BUFF_SZ];
	char new_filename_buff[API_PROGRAM_FILENAME_BUFF_SZ];



	for(int i = index; i<=programs_in_mem; i ++){

		char aux_name_itoa[4]={0};

	    strcpy(old_filename_buff, "");
	    strcat(old_filename_buff, API_PROGRAM_FILENAME_BASE);
	    itoa(i+1, aux_name_itoa, 10);
	    strcat(old_filename_buff, aux_name_itoa);
	    strcat(old_filename_buff, ".txt");

	    strcpy(new_filename_buff, "");
	    strcat(new_filename_buff, API_PROGRAM_FILENAME_BASE);
	    itoa(i, aux_name_itoa, 10);
	    strcat(new_filename_buff, aux_name_itoa);
	    strcat(new_filename_buff, ".txt");

	    if(false == self_config_->user_fs_renaming_func(old_filename_buff, new_filename_buff)){
	    	return false;
	    }

	}





#if 0
	self_config_->user_fs_renaming_func(old, new);
#endif

    return false;
}


uint32_t api_program_manager_get_n_programs_in_mem(void){

	uint32_t count =0;
	api_program_manager_program_t aux_program;

	for(int i=1; i<API_PROGRAM_MAX_PROGRAM_INDEX; i++){
		if(true ==  api_program_manager_read(&aux_program, i)){
			count++;
		}
		else{
			break;
		}
	}

	return count;
}


void  api_program_manager_set_configuration(api_program_manager_pf_config_t* p_config){

	self_config_ = p_config;
}

void  api_program_manager_init(void){

	config_api_program_manager();

}
void  api_program_manager_deinit(void){

}
void  api_program_manager_loop(void){

}

/********************** end of file ******************************************/

/** @}Final Doxygen */
