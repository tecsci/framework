/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		xsx.c
* @date		1 oct. 2021
* @author
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
*
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/

#include "config_api_program_manager.h"

#include "api_program_manager.h"
#include "board_spiffs.h"
#include "rsc_json_lib.h"
#include "esp_log.h"


/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

api_program_manager_pf_config_t config_;

static const char *TAG = "api_program_manager_config";

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

bool config_json_serialize_func(api_program_manager_program_t * program, char * buffer, uint32_t buffer_size){

	rsc_json_lib_t *nuevo_json;
	rsc_json_lib_create(&nuevo_json);


	if (false == rsc_json_lib_add_data(nuevo_json, "name" , program->name)){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}


	char aux_itoa[4];
	itoa(program->v1,aux_itoa,10);
	if (false == rsc_json_lib_add_data(nuevo_json, "velocidad1" , aux_itoa)){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}


	//3:serializar
	if( false == rsc_json_lib_serialize(nuevo_json, buffer, buffer_size)){
		ESP_LOGE(TAG, "error serializing");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	rsc_json_lib_delete(nuevo_json);
	return true;
}

bool config_json_deserialize_func(api_program_manager_program_t * program, char * buffer, uint32_t buffer_size){


	rsc_json_lib_t *json_parseado;
	rsc_json_lib_deserialize(&json_parseado, buffer, buffer_size);



	if(false == rsc_json_lib_get_item_data_by_name(json_parseado, "name" , program->name, API_PROGRAM_CONFIG_NAME_SIZE)){
		rsc_json_lib_delete(json_parseado);
		return false;
	}


	char aux_atoi[4];
	if(false == rsc_json_lib_get_item_data_by_name(json_parseado,  "velocidad1", aux_atoi, 4)){
		rsc_json_lib_delete(json_parseado);
		return false;
	}
	program->v1 =(uint32_t)atoi(aux_atoi);


	rsc_json_lib_delete(json_parseado);

	return true;

}


bool config_spiffs_writing_func(const char * path , char * buffer, uint32_t buffer_size){

	if(true == board_spiffs_is_mounted()){

    	FILE* f;
        f = board_spiffs_open(path,"w");

        if(NULL == f){
            board_spiffs_close(f);
            ESP_LOGE(TAG, "error opening file");
    		return false;
        }

        board_spiffs_write(f,buffer);
        board_spiffs_write(f,"\n");

        board_spiffs_close(f);

		return true;
	}

	ESP_LOGE(TAG, "error spiffs not mounted ");

	return false;
}

bool config_spiffs_reading_func(const char * path , char * buffer, uint32_t buffer_size){

	if(true == board_spiffs_is_mounted()){

    	FILE* f;
        f = board_spiffs_open(path,"r");

        if(NULL == f){
            board_spiffs_close(f);
    		return false;
        }

        board_spiffs_read_line(f, buffer, buffer_size);

        board_spiffs_close(f);
		return true;
	}

	return false;

}

bool config_spiffs_deleting_func(const char * path ){

	if(true == board_spiffs_is_mounted()){
		return board_spiffs_delete_file(path);
	}

	return false;
}

bool config_spiffs_renaming_func(const char * path_old, const char * path_new ){

	if(true == board_spiffs_is_mounted()){
		return board_spiffs_rename_file(path_old, path_new);
	}

	return false;
}


void config_api_program_manager(void){

	config_.user_serialize_func=config_json_serialize_func;
	config_.user_deserialize_func=config_json_deserialize_func;
	config_.user_fs_writing_func=config_spiffs_writing_func;
	config_.user_fs_reading_func=config_spiffs_reading_func;
	config_.user_fs_deleting_func=config_spiffs_deleting_func;
	config_.user_fs_renaming_func=config_spiffs_renaming_func;

	api_program_manager_set_configuration(&config_);

}


/********************** end of file ******************************************/

/** @}Final Doxygen */
