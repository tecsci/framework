/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		api_wifi.c
* @date		9 nov. 2021
* @author	
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/******** inclusions ***************/

#include "include/api_wifi.h"

/******** macros and definitions ***********/

/******** internal data declaration **********/

/******** internal data definition ***********/

/******** external data definition ***********/

/******** internal functions definition ********/

static struct{

	uint16_t n_scaned;
	api_wifi_record_t wifi_records[API_WIFI_SCAN_MAX];
	uint32_t timestamp;

}self_scan_;

static struct{
	bool reconnect_loop_enable;
}self_;

/******** external functions definition ********/

/*SCAN FUNCS*/
bool api_wifi_scan_get_wifi_record(uint16_t n, api_wifi_record_t* p_wifi_records ){
	//OBS: scanned wifi starts at 1
	if( n >self_scan_.n_scaned){
		return false;
	}
	if( n == 0 ){
		return false;
	}

	*p_wifi_records = self_scan_.wifi_records[n-1];
	return true;
}

uint16_t api_wifi_scan_get_n(){

	return self_scan_.n_scaned;
}

uint32_t api_wifi_scan_get_timestamp(){

	return self_scan_.timestamp;
}

void api_wifi_scan(){
	self_scan_.n_scaned = board_wifi_scan((board_wifi_record_t*)self_scan_.wifi_records, API_WIFI_SCAN_MAX);
	self_scan_.timestamp = board_timer_get_time_ms();
}

/*STA CONNECTION FUNCS*/

bool  api_wifi_sta_connect(char* ssid, char* pass){
	return  board_wifi_sta_connect(ssid, pass);
}

bool  api_wifi_sta_reconnect(void){
	return  board_wifi_sta_reconnect();
}

bool  api_wifi_sta_is_connected(void){
	return  board_wifi_sta_is_connected();
}

bool api_wifi_sta_disconnect(){
	return board_wifi_sta_disconnect();
}

/*API STD FUNCS*/
void api_wifi_init(){
	self_scan_.n_scaned=0;
	self_scan_.timestamp=0;
	self_.reconnect_loop_enable=false;
}
void api_wifi_deinit(){

}
void api_wifi_loop(){

	if( true == self_.reconnect_loop_enable){
    	if(false == api_wifi_sta_is_connected()){
    		 api_wifi_sta_reconnect();
    	}
	}

}

/******** end of file **************/

/** @}Final Doxygen */
