/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		xsx.c
* @date		1 oct. 2021
* @author
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
*
* @brief
* @{ Init Doxygen
*/

#ifndef API_PROGRAM_MANAGER_CONFIG_H_
#define API_PROGRAM_MANAGER_CONFIG_H_

/********************** inclusions *******************************************/
#include "stdint.h"
/********************** macros ***********************************************/

#define API_PROGRAM_CONFIG_NAME_SIZE 20

/********************** typedef **********************************************/


typedef struct{

	char name[API_PROGRAM_CONFIG_NAME_SIZE];
	uint32_t v1;

}config_api_program_t;

typedef config_api_program_t api_program_manager_program_t;

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void  config_api_program_manager(void);

#endif /* API_PROGRAM_MANAGER_CONFIG_H_ */
/********************** end of file ******************************************/

/** @}Final Doxygen */
