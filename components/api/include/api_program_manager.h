/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		xsx.c
* @date		1 oct. 2021
* @author
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
*
* @brief
* @{ Init Doxygen
*/

#ifndef API_PROGRAM_MANAGER_H_
#define API_PROGRAM_MANAGER_H_

/********************** inclusions *******************************************/
#include "config_api_program_manager.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"
#include "stdbool.h"



/********************** macros ***********************************************/


#define API_PROGRAM_MAX_PROGRAM_INDEX       (50)

#define API_PROGRAM_DATA_BUFF_SZ 			(512)
#define API_PROGRAM_FILENAME_BUFF_SZ 		(50)

#define API_PROGRAM_FILENAME_BASE 	        "/spiffs/program_"

/********************** typedef **********************************************/

#if 0
typedef struct{

	char nombre_programa[]
	uint32_t v1;
	uint32_t a1;
	uint32_t v2;
	uint32_t a2;

}api_program_manager_program_t;
#endif

typedef bool (*user_serialize_func_t)(api_program_manager_program_t *, char *, uint32_t);
typedef bool (*user_deserialize_func_t)(api_program_manager_program_t *, char *, uint32_t);
typedef bool (*user_fs_writing_func_t)(const char *,char *, uint32_t);
typedef bool (*user_fs_reading_func_t)(const char *,char *, uint32_t);
typedef bool (*user_fs_deleting_func_t)(const char *);
typedef bool (*user_fs_renaming_func_t)(const char *, const char *);

typedef struct{

	user_serialize_func_t user_serialize_func;
	user_deserialize_func_t user_deserialize_func;
	user_fs_writing_func_t user_fs_writing_func;
	user_fs_reading_func_t user_fs_reading_func;
	user_fs_deleting_func_t user_fs_deleting_func;
	user_fs_renaming_func_t user_fs_renaming_func;

}api_program_manager_pf_config_t;

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

bool api_program_manager_write(api_program_manager_program_t * program, uint8_t index);
bool  api_program_manager_read(api_program_manager_program_t * program, uint8_t numberprogram);
bool api_program_manager_delete_and_leftshift(uint8_t index);
uint32_t api_program_manager_get_n_programs_in_mem(void);
bool api_program_manager_write_in_empty(api_program_manager_program_t * program);


#if 0 //WIP
bool api_program_manager_write_empty_slot(api_program_manager_program_t * program);
bool api_program_manager_read_backup(api_program_manager_program_t *program);
bool api_program_manager_write_backup(api_program_manager_program_t *program);
#endif

void  api_program_manager_set_configuration(api_program_manager_pf_config_t* p_config);
void  api_program_manager_init(void);
void  api_program_manager_deinit(void);
void  api_program_manager_loop(void);


#endif /* API_PROGRAM_MANAGER_H_ */
/********************** end of file ******************************************/

/** @}Final Doxygen */
