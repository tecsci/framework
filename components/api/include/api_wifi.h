/* 
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		api_wifi.h
* @date		9 nov. 2021
* @author	
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef _API_WIFI_H_
#define _API_WIFI_H_
/******** inclusions ***************/

#include "board.h"

/******** macros *****************/

#define API_WIFI_SCAN_MAX 20

/******** typedef ****************/

typedef  board_wifi_record_t api_wifi_record_t;

/******** external data declaration **********/

/******** external functions declaration *********/

/*SCAN FUNCS*/
bool api_wifi_scan_get_wifi_record(uint16_t n, api_wifi_record_t* p_wifi_records );
uint16_t api_wifi_scan_get_n();
uint32_t api_wifi_scan_get_timestamp();
void api_wifi_scan();

/*STA CONNECTION FUNCS*/
bool  api_wifi_sta_connect(char* ssid, char* pass);
bool  api_wifi_sta_reconnect(void);
bool  api_wifi_sta_is_connected(void);
bool  api_wifi_sta_disconnect(void);

/*API STD FUNCS*/
void api_wifi_init();
void api_wifi_deinit();
void api_wifi_loop();

#endif /* _API_WIFI_H_ */
/** @}Final Doxygen */
