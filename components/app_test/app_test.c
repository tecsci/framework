/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		app_test.c
* @date		14 jun. 2021
* @author	
*		-Martin Abel Gambarotta   magambarotta@gmail.com
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "app_test.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "board.h"
#include "api.h"

#include "app_hmi.h"
#include "api_program_manager.h"
#include "config_api_program_manager.h"

/********************** macros and definitions *******************************/

#define EXAMPLE_TEST_PORT_SERIAL	0
#define EXAMPLE_TEST_PORT_SPI		0
#define EXAMPLE_TEST_BME_280		1
#define	EXAMPLE_TEST_GPIO_BLINK		1
#define EXAMPLE_TEST_SAVED_PROGRAMS_APP_HMI 0
#define EXAMPLE_TEST_WIFI_SCAN 0

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/
static const char *TAG = "app_test:";
/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/
void app_test_init(void)
{
	xTaskCreate(app_test_loop, "test task" 	// A name just for humans
			, (1024)*8 							// This stack size can be checked & adjusted by reading the Stack Highwater
			, NULL, 2 						// Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
			, NULL);

}
void app_test_deinit(void)
{}
void app_test_loop(void *pvParameters) {

#if EXAMPLE_TEST_GPIO_BLINK
static bool state=true;
#endif

//TEST SAVED PROGRAMS APP HMI
#if EXAMPLE_TEST_SAVED_PROGRAMS_APP_HMI
      if(false == board_spiffs_is_mounted()){
      	ESP_LOGE(TAG,"spiffs not mounted");
      }
      else{
      	ESP_LOGI(TAG,"spiffs mounted");
      }

      //WRITE PROGRAM
      api_program_manager_program_t program;
      strcpy(program.name,"hector");
      program.v1=55;
      if( false == api_program_manager_write(&program, 1)){
      	ESP_LOGE(TAG, "error api program write 1");
      }

      strcpy(program.name,"william ");
      program.v1=92;
      if( false == api_program_manager_write(&program, 2)){
      	ESP_LOGE(TAG, "error api program write 2");
      }

      strcpy(program.name,"william mort");
      program.v1=92;
      if( false == api_program_manager_write(&program, 3)){
      	ESP_LOGE(TAG, "error api program write 3");
      }

      strcpy(program.name,"williana");
      program.v1=92;
      if( false == api_program_manager_write(&program, 4)){
      	ESP_LOGE(TAG, "error api program write 4");
      }

      strcpy(program.name,"williane");
      program.v1=92;
      if( false == api_program_manager_write(&program, 5)){
      	ESP_LOGE(TAG, "error api program write 5");
      }

      strcpy(program.name,"hetor");
      program.v1=92;
      if( false == api_program_manager_write(&program, 6)){
      	ESP_LOGE(TAG, "error api program write 6");
      }

      strcpy(program.name,"hetora");
      program.v1=92;
      if( false == api_program_manager_write(&program, 7)){
      	ESP_LOGE(TAG, "error api program write 7");
      }

      strcpy(program.name,"hetora2");
      program.v1=92;
      if( false == api_program_manager_write(&program, 8)){
      	ESP_LOGE(TAG, "error api program write 78");
      }


      api_program_manager_delete_and_leftshift(3);


      app_hmi_goto_state_window_programs();

      int aux_next= 0;
#endif

  //TEST  WIFI SCAN
  #if EXAMPLE_TEST_WIFI_SCAN
      board_wifi_record_t ap_records[20];
      uint16_t scan_rst=0;
#endif

      for(;;){

//TEST BLINK
#if EXAMPLE_TEST_GPIO_BLINK
      state=!state;
      api_gpio_write(API_GPIO_PIN_12, state);

#endif

//TEST UART_1
#if EXAMPLE_TEST_PORT_SERIAL
      board_serial_write(API_UART_1,"HOLA HECTOR DESDE UART1\r\n");
#endif


//TEST SPI2
#if EXAMPLE_TEST_PORT_SPI
      api_tmc_write_read(&tx,&rx,10);
#endif


//TEST SAVED PROGRAMS APP HMI
#if EXAMPLE_TEST_SAVED_PROGRAMS_APP_HMI
if(aux_next == 5){
	ESP_LOGI(TAG,"next page!");
    widget_next_page_action_handler_();
}
if(aux_next == 10){
	ESP_LOGI(TAG,"prev page!");
	widget_prev_page_action_handler_();
	aux_next=0;
}
	aux_next++;
#endif

//TEST WIFI SCAN
#if EXAMPLE_TEST_WIFI_SCAN
    scan_rst=board_wifi_scan(ap_records, 20);
    if( 0 == scan_rst){
    	ESP_LOGI(TAG , "no se pudo scanear nada");
    }
    else{
	 ESP_LOGI(TAG, "Found %d access points:\n", scan_rst);
	 for(int i = 0; i < scan_rst; i++){
		 ESP_LOGI(TAG, "Nombre %d: %32s", i , ap_records[i].ssid);
   }
}

#endif

      vTaskDelay(200/ portTICK_PERIOD_MS);

  }
}

/********************** end of file ******************************************/

/** @}Final Doxygen */
