/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		xsx.c
* @date		1 oct. 2021
* @author
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
*
* @brief
* @{ Init Doxygen
*/


#ifndef RSC_JSON_LIB_HPP_
#define RSC_JSON_LIB_HPP_

/********************** inclusions *******************************************/

#include "cJSON.h"

#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <string.h>

/********************** macros ***********************************************/

/********************** typedef **********************************************/

typedef cJSON rsc_json_lib_t;

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void rsc_json_lib_create(rsc_json_lib_t ** pp_json);
void rsc_json_lib_delete(rsc_json_lib_t * p_json);
bool rsc_json_lib_add_data(rsc_json_lib_t * p_json, const char* name, const char* data);
bool rsc_json_lib_add_number(rsc_json_lib_t * p_json, const char* name,  double data);
bool rsc_json_lib_add_bool(rsc_json_lib_t * p_json, const char* name, bool data);
bool rsc_json_lib_serialize( rsc_json_lib_t * p_json, char* buff, uint32_t buff_size);
bool rsc_json_lib_deserialize(rsc_json_lib_t ** pp_json,char * buffer, uint32_t buff_size);
uint32_t rsc_json_lib_get_size(rsc_json_lib_t * p_json);
bool rsc_json_lib_get_n_item_data(rsc_json_lib_t * p_json, uint32_t n, char * buffer, uint32_t buff_size );
bool rsc_json_lib_get_n_item_name(rsc_json_lib_t * p_json, uint32_t n, char * buffer, uint32_t buff_size );
bool rsc_json_lib_get_item_data_by_name(rsc_json_lib_t * p_json, char * name, char * buffer, uint32_t buff_size );


#endif /* RSC_JSON_LIB_HPP_ */
/********************** end of file ******************************************/
/** @}Final Doxygen */
