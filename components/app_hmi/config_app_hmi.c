/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		app_hmi_config.c
* @date		14 oct. 2021
* @author	
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/


//ACA PODRIAN IR LAS VENTANAS CUSTOM (osea que no son del framework)
//del framework pueden ser:
//programas
//manejo manual
//settings

//tambien aca pueden estar los nombres de los botones


/******** inclusions ***************/

/******** macros and definitions ***********/

/******** internal data declaration **********/

/******** internal data definition ***********/

/******** external data definition ***********/

/******** internal functions definition ********/

/******** external functions definition ********/
void app_hmi_config_init(){}
void app_hmi_config_deinit(){}
void app_hmi_config_loop(){}

/******** end of file **************/

/** @}Final Doxygen */
