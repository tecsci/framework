/* 
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		app_hmi_config.h
* @date		14 oct. 2021
* @author	
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef _APP_HMI_CONFIG_H_
#define _APP_HMI_CONFIG_H_
/******** inclusions ***************/

/******** macros *****************/

/******** typedef ****************/

/******** external data declaration **********/

/******** external functions declaration *********/

void app_hmi_config_init();
void app_hmi_config_deinit();
void app_hmi_config_loop();

#endif /* _APP_HMI_CONFIG_H_ */
/** @}Final Doxygen */
