/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* Copyright (C) <2021>  <Martin Abel Gambarotta   magambarotta@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		app_hmi.h
* @date		14 jun. 2021
* @author	
*		-martin
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

#ifndef _APP_HMI_H_
#define _APP_HMI_H_
/********************** inclusions *******************************************/

/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/
void app_hmi_init(void);
void app_hmi_deinit(void);
void app_hmi_loop(void *pvParameters);

//FOR TESTING
//Goto Funcs
void app_hmi_goto_state_window_programs(void);
void app_hmi_goto_state_window_menu(void);
void app_hmi_goto_state_window_manual(void);
void app_hmi_goto_state_window_settings(void);
//Test
void widget_test_action_handler_(void);
//Menu
void widget_menu_to_programs_handler_(void);
void widget_menu_to_new_handler_(void);
void widget_menu_to_manual_handler_(void);
void widget_menu_to_settings_handler_(void);
void widget_menu_to_running_handler_(void);
void widget_menu_to_info_handler_(void);
//Programs
void widget_next_page_action_handler_(void);
void widget_prev_page_action_handler_(void);
void widget_programs_to_menu_handler_(void);
void widget_slot1_selected_handler_(void);
void widget_slot2_selected_handler_(void);
void widget_slot3_selected_handler_(void);
void widget_slot4_selected_handler_(void);
void widget_slot5_selected_handler_(void);
void widget_programs_to_new_handler_(void);
//Selected
void widget_play_selected_handler_(void);
void widget_delete_selected_handler_(void);
void widget_selected_to_menu_handler_(void);
//Running
void widget_running_to_menu_handler_(void);
//New
void widget_new_to_menu_handler_(void);
void widget_new_play_handler_(void);
void widget_new_save_handler_(void);
void widget_new_name_handler_(void);
void widget_new_var1_handler_(void);
//Info
void widget_info_to_menu_handler_(void);
//Settings
void widget_settings_to_menu_handler_(void);
void widget_settings_reset_m_handler_(void);
void widget_settings_to_wifi_handler_(void);
//Manual
void widget_manual_to_menu_handler_(void);
//Wifi
void widget_wifi_to_settings_handler_(void);
void widget_wifi_s1_handler_(void);
void widget_wifi_s2_handler_(void);
void widget_wifi_s3_handler_(void);
void widget_wifi_s4_handler_(void);
void widget_wifi_next_page_handler_(void);
void widget_wifi_prev_page_handler_(void);
void widget_wifi_disconnect_handler_(void);
//Wifi Connect
void widget_wificonnect_ok_handler_(void);
void widget_wificonnect_to_menu_handler_(void);
void widget_wificonnect_password_handler_(void);

#endif /* _APP_HMI_H_ */
/** @}Final Doxygen */

