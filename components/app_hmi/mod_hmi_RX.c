/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		mod_hmi_RX.c
* @date		22 sep. 2021
* @author	
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/******** inclusions ***************/

#include "mod_hmi_RX.h"

#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "api_serial.h"

/******** macros and definitions ***********/

#define MOD_HMI_RX_UART_NUM API_UART_1
#define MOD_HMI_RX_BYTE_READ_VERBOSE true

static struct{

	mod_hmi_RX_command_t command_RX;

} self_;

static const char*TAG ="mod_hmi_RX";

/******** internal data declaration **********/

/******** internal data definition ***********/

const uint8_t header_code_[MOD_HMI_RX_HEADER_SZ]=
												{MOD_HMI_RX_HEADER_B1,
												  MOD_HMI_RX_HEADER_B2,
												  MOD_HMI_RX_HEADER_B3};

const uint8_t tail_code_[MOD_HMI_RX_TAIL_SZ]=
												{MOD_HMI_RX_TAIL_B1,
												  MOD_HMI_RX_TAIL_B2,
												  MOD_HMI_RX_TAIL_B3};

/******** external data definition ***********/

QueueHandle_t mod_hmi_RX_queue = NULL;

/******** internal functions definition ********/

static bool read_n_bytes_(uint32_t n , uint8_t* p_buff, bool verbose){

	for(int i =0; i < n; i++){
		if(0 == api_serial_read(MOD_HMI_RX_UART_NUM, &p_buff[i])){
			ESP_LOGI(TAG,"read_n_bytes fail: %d", i);
			return false;
		}

		if( true == verbose){
			ESP_LOGI(TAG,"read_n_bytes %d: 0x%X \n", i, p_buff[i]);
		}
	}

	return true;
}

static bool messages_available_(){

	return api_serial_is_available(MOD_HMI_RX_UART_NUM);
}

static bool header_check_(){

	static uint8_t aux_byte;

	for(int i =0; i< MOD_HMI_RX_HEADER_SZ; i++){

		if(false == read_n_bytes_(1,&aux_byte,MOD_HMI_RX_BYTE_READ_VERBOSE)){
			ESP_LOGE(TAG, "header fail read");
			return false;
		}

		if(header_code_[i] != aux_byte){
			ESP_LOGE(TAG, "header parse false");
			return false;
		}
	}
	return true;

}

static bool tail_check_(){

	static uint8_t aux_byte;

	for(int i =0; i< MOD_HMI_RX_TAIL_SZ; i++){

		if(false == read_n_bytes_(1,&aux_byte,MOD_HMI_RX_BYTE_READ_VERBOSE)){
			ESP_LOGE(TAG, "tail fail read");
			return false;
		}

		if(tail_code_[i] != aux_byte){
			ESP_LOGE(TAG, "tail parse false");
			return false;
		}

	}
	return true;

}

static bool crc_check_(){

	static uint8_t aux_bytes[2];

	//READ 3 BYTES
	if(false == read_n_bytes_(2,aux_bytes,MOD_HMI_RX_BYTE_READ_VERBOSE)){
		ESP_LOGI(TAG, "crc fail read");
		return false;
	}

	return true;
}

static bool parse_command_code_(uint32_t* p_buff){

	uint8_t aux_bytes[MOD_HMI_RX_CMD_CODE_SZ] = {0};

	if(true == read_n_bytes_(MOD_HMI_RX_CMD_CODE_SZ, aux_bytes,MOD_HMI_RX_BYTE_READ_VERBOSE)){

		*p_buff = (aux_bytes[0] << 8) | (aux_bytes[1]);

		return true;

	}

	return false;

}


static bool parse_data_len_(uint32_t* p_buff){

	uint8_t aux_bytes[2] = {0,0};

	if(true == read_n_bytes_(2, aux_bytes,MOD_HMI_RX_BYTE_READ_VERBOSE)){

		*p_buff = (aux_bytes[0]) << 8 |  (aux_bytes[1]);

		return true;

	}

	return false;

}
/******** external functions definition ********/
void mod_hmi_RX_init(void) {

	//task create
    xTaskCreate(
    		mod_hmi_RX_task_loop
      ,  "hmi RX task"   // A name just for humans
      ,  4096  // This stack size can be checked & adjusted by reading the Stack Highwater
      ,  NULL
      ,  2  // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
      ,  NULL
      );

}

void mod_hmi_RX_deinit(void) {


}

/*
 * COMMANDS: HEADER + CMD_CODE + DATA_LEN + DATA + TAIL + CRC
 */
void mod_hmi_RX_task_loop(void *argument) {

	mod_hmi_RX_queue = xQueueCreate( 3 , sizeof( mod_hmi_RX_command_t ) );

	   if( ( mod_hmi_RX_queue == NULL ) ){
			for(;;){
			   	  ESP_LOGE(TAG, "mod_hmi_RX_task_loop: error creating mod_hmi_RX_queue");
				vTaskDelay(10000/ portTICK_PERIOD_MS);
			}
		}

	for(;;){


		//CHECK COMM
		if (true == messages_available_()) {
			ESP_LOGI(TAG,"msg available");

			//HEADER CHECK STATE
			if(true == header_check_() ){

				//ESP_LOGI(TAG,"header ok\n");

				//PARSE COMMAND

				//READ COMMAND CODE
				if(true == parse_command_code_(&self_.command_RX.command_code)){

					//ESP_LOGI(TAG, "command code: 0x%X\n", self_.command_RX.command_code);

					//READ DATA LEN
					if(true == parse_data_len_(&self_.command_RX.len)){

						//ESP_LOGI(TAG, "data len %d:",self_.command_RX.len);

						//READ MSG DATA
						if( true == read_n_bytes_(self_.command_RX.len,self_.command_RX.data,MOD_HMI_RX_BYTE_READ_VERBOSE)){
							//mensaje leido ok

							if( true == tail_check_() ){

								if( true == crc_check_() ){
									//SEND QUEUE
									ESP_LOGI(TAG, "SEND DATA QUEUE");
									if(NULL != mod_hmi_RX_queue ){
										xQueueGenericSend(mod_hmi_RX_queue, &self_.command_RX,0U, 0U);
									}

									//clear data
									self_.command_RX.command_code=0;
									self_.command_RX.len=0;
									memset(self_.command_RX.data, 0, MOD_HMI_RX_DATA_MAX_SZ);
								}
							}

					}

				}

			}

		}
		}

		vTaskDelay(50/ portTICK_PERIOD_MS);
	}

}

/******** end of file **************/

/** @}Final Doxygen */
