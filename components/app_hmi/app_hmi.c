/*
* <TECSCI Dip Coater     Technology for Science   info@tecsci.com.ar>
* Copyright (C) <2021>  <Martin Abel Gambarotta   magambarotta@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		app_hmi.c
* @date		12 Oct. 2021
* @author	
*		-Martin Abel Gambarotta   magambarotta@gmail.com
*		-Lucas Mancini						mancinilucas95@gmail.com
* @version	v1.0.2
* @brief
*
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "app_hmi.h"
#include "driver/uart.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "math.h"


#include "api.h"
#include "board.h"
#include "api_stone_hmi.h"
#include "config_api_program_manager.h"
#include "mod_hmi_RX.h"
#include "rsc_statemachine.h"



/********************** macros and definitions *******************************/

#define APP_HMI_WIDGET_NAME_SZ 30
#define APP_HMI_WIDGET_EDITTEXT_DATA_SIZE 50

#define APP_HMI_N_PROGRAMS_PER_PAGE 3
#define APP_HMI_N_WIFI_PER_PAGE 4

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

typedef struct{

	char name[APP_HMI_WIDGET_NAME_SZ];
	void (*handler)(void);

}app_hmi_widget_t;

/*WIDGETS ENUM*/
typedef enum{

	//Test
	APP_HMI_WIDGET_TEST=0,

	//Menu
	APP_HMI_WD_MENU_TO_MANUAL,
	APP_HMI_WD_MENU_TO_PROGRAMS,
	APP_HMI_WD_MENU_TO_RUNNING,
	APP_HMI_WD_MENU_TO_NEW,
	APP_HMI_WD_MENU_TO_INFO,
	APP_HMI_WD_MENU_TO_SETTINGS,

	//Manual
	APP_HMI_WD_MANUAL_TO_MENU,

	//Saved programs
	APP_HMI_WD_PROGRAMS_NEXT_PAGE,
	APP_HMI_WD_PROGRAMS_PREV_PAGE,
	APP_HMI_WD_PROGRAMS_TO_MENU,
	APP_HMI_WD_SLOT1_SELECTED,
	APP_HMI_WD_SLOT2_SELECTED,
	APP_HMI_WD_SLOT3_SELECTED,
	APP_HMI_WD_SLOT4_SELECTED,
	APP_HMI_WD_SLOT5_SELECTED,

	//Selected
	APP_HMI_WD_PLAY_SELECTED,
	APP_HMI_WD_DELETE_SELECTED,
	APP_HMI_WD_SELECTED_TO_MENU,

	//Running
	APP_HMI_WD_RUNNING_TO_MENU,

	//New
	APP_HMI_WD_PROGRAMS_TO_NEW,
	APP_HMI_WD_NEW_TO_MENU,
	APP_HMI_WD_PLAY_NEW,
	APP_HMI_WD_SAVE_NEW,
	APP_HMI_WD_NEW_NAME,
	APP_HMI_WD_NEW_VAR1,

	//Info
	APP_HMI_WD_INFO_TO_MENU,

	//Settings
	APP_HMI_WD_SETTINGS_TO_MENU,
	APP_HMI_WD_SETTINGS_RESET_MACHINE,
	APP_HMI_WD_SETTINGS_TO_WIFI,

	//Wifi
	APP_HMI_WD_WIFI_TO_SETTINGS,
	APP_HMI_WD_WIFI_NEXT_PAGE,
	APP_HMI_WD_WIFI_PREV_PAGE,
	APP_HMI_WD_WIFI_DISCONNECT,
	APP_HMI_WD_WIFI_S1,
	APP_HMI_WD_WIFI_S2,
	APP_HMI_WD_WIFI_S3,
	APP_HMI_WD_WIFI_S4,

	//Wifi connect
	APP_HMI_WD_WIFICONNECT_OK,
	APP_HMI_WD_WIFICONNECT_TO_MENU,
	APP_HMI_WD_WIFICONNECT_PASSWORD,


	APP_HMI_WIDGET_TAG__N
}app_hmi_widget_tags_t;


static struct{

	//STATE MACHINE
	rsc_statemachine_t statemachine;

	//WIDGET DATA
	app_hmi_widget_t widgets[APP_HMI_WIDGET_TAG__N];
	char wd_name[APP_HMI_WIDGET_NAME_SZ];
	char wd_text[APP_HMI_WIDGET_EDITTEXT_DATA_SIZE];
	int32_t wd_int;
	api_stone_rx_button_val_t wd_bt;

	//RX DATA
	mod_hmi_RX_command_t command_RX;

}self_;


static struct{
	bool dumy;
}window_menu_vars_;

static struct{
	bool dumy;
}window_manual_vars_;

static struct{
	bool dumy;
}window_setting_vars_;

static struct{
	bool dumy;
}window_info_vars_;

static struct{
	uint8_t actual_page;
	bool next_page;
	bool previous_page;

}window_programs_vars_;

static struct{
	uint8_t selected;
	uint8_t triger_slotn;
	bool play;
	bool delete;
}window_selected_program_vars_;

static struct{
	bool play;
	bool save;
	api_program_manager_program_t program;

}window_new_program_vars_;

static struct{
	 int32_t varx;
	 int32_t vary;

}window_running_program_vars_;

static struct{

	uint8_t actual_page;
	bool next_page;
	bool previous_page;

}window_wifi_vars_;

static struct{
	uint8_t selected;
	uint8_t triger_slotn;
	bool ok;
	char password[30];
}window_wifi_connect_vars_;

static const char *TAG = "app_hmi:";
/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/*STATE MACHINE*/
static void state_window_menu_(rsc_statemachine_t *sm);
static void state_window_manual_(rsc_statemachine_t *sm);
static void state_window_settings_(rsc_statemachine_t *sm);
static void state_window_info_(rsc_statemachine_t *sm);
static void state_window_programs_(rsc_statemachine_t *sm);
static void state_window_selected_program_(rsc_statemachine_t *sm);
static void state_window_new_program_(rsc_statemachine_t *sm);
static void state_window_running_program_(rsc_statemachine_t *sm);
static void state_window_wifi_(rsc_statemachine_t *sm);
static void state_window_wifi_connect_(rsc_statemachine_t *sm);

/*WIDGET ACTION CONFIG*/
static void widget_actions_config_(void) {

	//TEST WIDGETS
	strncpy(self_.widgets[APP_HMI_WIDGET_TEST].name,
			"test", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WIDGET_TEST].handler=widget_test_action_handler_;

	//MENU WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_MENU_TO_PROGRAMS].name,
			"menu_to_programs", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MENU_TO_PROGRAMS].handler=widget_menu_to_programs_handler_;

	strncpy(self_.widgets[APP_HMI_WD_MENU_TO_RUNNING].name,
			"menu_to_running", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MENU_TO_RUNNING].handler=widget_menu_to_running_handler_;

	strncpy(self_.widgets[APP_HMI_WD_MENU_TO_INFO].name,
			"menu_to_info", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MENU_TO_INFO].handler=widget_menu_to_info_handler_;

	strncpy(self_.widgets[APP_HMI_WD_MENU_TO_MANUAL].name,
			"menu_to_manual", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MENU_TO_MANUAL].handler=widget_menu_to_manual_handler_;

	strncpy(self_.widgets[APP_HMI_WD_MENU_TO_NEW].name,
			"menu_to_new", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MENU_TO_NEW].handler=widget_menu_to_new_handler_;

	//PROGRAMS WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_PROGRAMS_NEXT_PAGE].name,
			"programs_next_page", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_PROGRAMS_NEXT_PAGE].handler=widget_next_page_action_handler_;

	strncpy(self_.widgets[APP_HMI_WD_PROGRAMS_PREV_PAGE].name,
			"programs_prev_page", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_PROGRAMS_PREV_PAGE].handler=widget_prev_page_action_handler_;

	strncpy(self_.widgets[APP_HMI_WD_PROGRAMS_TO_MENU].name,
			"programs_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_PROGRAMS_TO_MENU].handler=widget_programs_to_menu_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SLOT1_SELECTED].name,
			"p_slot1_selected", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SLOT1_SELECTED].handler=widget_slot1_selected_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SLOT2_SELECTED].name,
			"p_slot2_selected", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SLOT2_SELECTED].handler=widget_slot2_selected_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SLOT3_SELECTED].name,
			"p_slot3_selected", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SLOT3_SELECTED].handler=widget_slot3_selected_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SLOT4_SELECTED].name,
			"p_slot4_selected", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SLOT4_SELECTED].handler=widget_slot4_selected_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SLOT5_SELECTED].name,
			"p_slot5_selected", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SLOT5_SELECTED].handler=widget_slot5_selected_handler_;

	strncpy(self_.widgets[APP_HMI_WD_PROGRAMS_TO_NEW].name,
			"programs_to_new", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_PROGRAMS_TO_NEW].handler=widget_programs_to_new_handler_;

	//SELECTED WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_PLAY_SELECTED].name,
			"selected_play", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_PLAY_SELECTED].handler=widget_play_selected_handler_;

	strncpy(self_.widgets[APP_HMI_WD_DELETE_SELECTED].name,
			"selected_delete", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_DELETE_SELECTED].handler=widget_delete_selected_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SELECTED_TO_MENU].name,
			"selected_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SELECTED_TO_MENU].handler=widget_selected_to_menu_handler_;

	//RUNNING WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_RUNNING_TO_MENU].name,
			"running_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_RUNNING_TO_MENU].handler=widget_running_to_menu_handler_;

	//NEW WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_NEW_TO_MENU].name,
			"new_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_TO_MENU].handler=widget_new_to_menu_handler_;

	strncpy(self_.widgets[APP_HMI_WD_PLAY_NEW].name,
			"new_play", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_PLAY_NEW].handler=widget_new_play_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SAVE_NEW].name,
			"new_save", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SAVE_NEW].handler=widget_new_save_handler_;

	strncpy(self_.widgets[APP_HMI_WD_NEW_NAME].name,
			"new_name", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_NAME].handler=widget_new_name_handler_;

	strncpy(self_.widgets[APP_HMI_WD_NEW_VAR1].name,
			"new_var1", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_VAR1].handler=widget_new_var1_handler_;


	//INFO WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_INFO_TO_MENU].name,
			"info_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_INFO_TO_MENU].handler=widget_info_to_menu_handler_;

	//SETTINGS WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_MENU_TO_SETTINGS].name,
			"menu_to_settings", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MENU_TO_SETTINGS].handler=widget_menu_to_settings_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SETTINGS_RESET_MACHINE].name,
			"settings_reset_m", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SETTINGS_RESET_MACHINE].handler=widget_settings_reset_m_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SETTINGS_TO_MENU].name,
			"settings_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SETTINGS_TO_MENU].handler=widget_settings_to_menu_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SETTINGS_TO_WIFI].name,
			"settings_to_wifi", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SETTINGS_TO_WIFI].handler=widget_settings_to_wifi_handler_;

	//MANUAL WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_MANUAL_TO_MENU].name,
			"manual_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MANUAL_TO_MENU].handler=widget_manual_to_menu_handler_;

	//WIFI WIDGETS

	strncpy(self_.widgets[APP_HMI_WD_WIFI_TO_SETTINGS].name,
			"wifi_to_settings", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_WIFI_TO_SETTINGS].handler=widget_wifi_to_settings_handler_;

	strncpy(self_.widgets[APP_HMI_WD_WIFI_S1].name,
			"wifi_p_slot1", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_WIFI_S1].handler=widget_wifi_s1_handler_;

	strncpy(self_.widgets[APP_HMI_WD_WIFI_S2].name,
			"wifi_p_slot2", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_WIFI_S2].handler=widget_wifi_s2_handler_;

	strncpy(self_.widgets[APP_HMI_WD_WIFI_S3].name,
			"wifi_p_slot3", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_WIFI_S3].handler=widget_wifi_s3_handler_;

	strncpy(self_.widgets[APP_HMI_WD_WIFI_S4].name,
			"wifi_p_slot4", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_WIFI_S4].handler=widget_wifi_s4_handler_;


	strncpy(self_.widgets[APP_HMI_WD_WIFI_NEXT_PAGE].name,
			"wifi_next_page", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_WIFI_NEXT_PAGE].handler=widget_wifi_next_page_handler_;

	strncpy(self_.widgets[APP_HMI_WD_WIFI_PREV_PAGE].name,
			"wifi_prev_page", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_WIFI_PREV_PAGE].handler=widget_wifi_prev_page_handler_;

	strncpy(self_.widgets[APP_HMI_WD_WIFI_DISCONNECT].name,
			"wifi_disconnecy", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_WIFI_DISCONNECT].handler=widget_wifi_disconnect_handler_;

	//WIFI CONNECT WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_WIFICONNECT_OK].name,
			"wificonnect_ok", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_WIFICONNECT_OK].handler=widget_wificonnect_ok_handler_;

	strncpy(self_.widgets[APP_HMI_WD_WIFICONNECT_TO_MENU].name,
			"wificonnect_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_WIFICONNECT_TO_MENU].handler=widget_wificonnect_to_menu_handler_;

	strncpy(self_.widgets[APP_HMI_WD_WIFICONNECT_PASSWORD].name,
			"wificonnect_pass", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_WIFICONNECT_PASSWORD].handler=widget_wificonnect_password_handler_;

}

//COMMAND PARSING
static void app_hmi_widget_parsing_(void)
{
	bool status_ok = true;
    //Loop
    if( mod_hmi_RX_queue != NULL )
 	   {
 	      if( xQueueReceive( mod_hmi_RX_queue, &self_.command_RX , 0 ) == pdPASS )
 	      {
#if 0
 	    	  ESP_LOGI(TAG, "command code: %d", self_.command_RX.command_code);
 	    	  ESP_LOGI(TAG, "len: %d", self_.command_RX.len);
#endif
 	        switch((api_stone_rx_code_t)  self_.command_RX.command_code ){
 	        	case API_STONE_RX_CODE_BUTTON:
 	        		ESP_LOGI(TAG, "button");
 	        		status_ok=api_stone_rx_process_button(self_.wd_name, &self_.wd_bt, self_.command_RX.data, self_.command_RX.len);
 	        		break;

 	        	case API_STONE_RX_CODE_EDIT_TEXT:
 	        		ESP_LOGI(TAG, "edit text");
 	        		status_ok =  api_stone_rx_process_edit_text(self_.wd_name, self_.wd_text, self_.command_RX.data, self_.command_RX.len);
 	        		break;

 	        	case API_STONE_RX_CODE_EDIT_INT:
 	        		ESP_LOGI(TAG, "edit int");
 	        		status_ok= api_stone_rx_process_edit_int(self_.wd_name, &self_.wd_int, self_.command_RX.data, self_.command_RX.len);
 	        		break;

 	        	case API_STONE_RX_CODE_LABEL_TEXT:
 	        		ESP_LOGI(TAG, "label int");
 	        		status_ok= api_stone_rx_process_edit_int(self_.wd_name, &self_.wd_int, self_.command_RX.data, self_.command_RX.len) ;
 	        		break;
 	        	default:
 	        		break;
 	        }


 	        if(true == status_ok){
 	        	ESP_LOGI(TAG, "name: %s", self_.wd_name);
				for(uint8_t i=0; i<APP_HMI_WIDGET_TAG__N ; i++){

					if(0 == strcmp(self_.wd_name, self_.widgets[i].name)){
						 self_.widgets[i].handler();
					}
				}
 	        }
 	        else{
 	        	ESP_LOGE(TAG, "cmd parsing - error procesing ");
 	        	status_ok=true;
 	        }

 	      }
 	   }
}

/*SPECIAL LOCAL FUNCS*/
static bool app_hmi_write_slot_name_(uint32_t slot_num, char* str, uint32_t data_len){

	static char aux_widget_name[15];
	static char aux_itoa[4];

	itoa(slot_num, aux_itoa, 10);
	strcpy(aux_widget_name, "slot_");
    strcat(aux_widget_name, aux_itoa);

	return api_stone_tx_set_text(aux_widget_name, API_STONE_SET_TEXT_LABEL, str, data_len);
}

static bool app_hmi_write_wifi_slot_name_(uint32_t slot_num, char* str, uint32_t data_len){

	static char aux_widget_name[15];
	static char aux_itoa[4];

	itoa(slot_num, aux_itoa, 10);
	strcpy(aux_widget_name, "wifi_s_");
    strcat(aux_widget_name, aux_itoa);

	return api_stone_tx_set_text(aux_widget_name, API_STONE_SET_TEXT_LABEL, str, data_len);
}

static bool app_hmi_write_selected_program_(api_program_manager_program_t program){

	if(false == api_stone_tx_set_text("selected_name", API_STONE_SET_TEXT_LABEL, program.name, strlen(program.name))){
		return false;
	}

	if(false == api_stone_tx_set_int("selected_val1", API_STONE_SET_TEXT_LABEL,program.v1 )){
		return false;
	}

	return true;

}

static bool app_hmi_write_selected_wifi_(api_wifi_record_t record){

	if(false == api_stone_tx_set_text("selected_wifi", API_STONE_SET_TEXT_LABEL, (char*)record.ssid, strlen((char*)record.ssid))){
		return false;
	}

	return true;

}

static bool app_hmi_write_wifi_status_(const char * str){

	if(false == api_stone_tx_set_text("wificonnect_status", API_STONE_SET_TEXT_LABEL, str, strlen(str))){
		return false;
	}

	return true;

}

static void app_hmi_run_program(api_program_manager_program_t program){


	ESP_LOGI(TAG,"funcion to run program: %s", program.name);

}

static void app_hmi_save_program(api_program_manager_program_t* p_program){

	ESP_LOGI(TAG,"funcion to run program: %s", p_program->name);

	api_program_manager_write_in_empty(p_program);

}

void app_hmi_delete_program(uint8_t index){

	api_program_manager_delete_and_leftshift(index);

}

void app_hmi_reset_machine(void){

	ESP_LOGI(TAG, "call function to reset all machine");
}

/********************** external functions definition ************************/

void app_hmi_goto_state_window_programs(void){
	rsc_statemachine_go_to(&self_.statemachine, state_window_programs_);
}

void app_hmi_goto_state_window_menu(void){
	rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
}

void app_hmi_goto_state_window_manual(void){
	rsc_statemachine_go_to(&self_.statemachine, state_window_manual_);
}

void app_hmi_goto_state_window_settings(void){
	rsc_statemachine_go_to(&self_.statemachine, state_window_settings_);
}

void app_hmi_init(void)
{
  xTaskCreate(app_hmi_loop, "app hmi task" 	// A name just for humans
  			, 4096 			// This stack size can be checked & adjusted by reading the Stack Highwater
  			, NULL, 2 		// Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
  			, NULL);
}
void app_hmi_deinit(void){

}
void app_hmi_loop(void *pvParameters)
{

	widget_actions_config_();
	rsc_statemachine_init(&self_.statemachine, state_window_menu_);
	api_stone_open_window("menu");

  for(;;) {

	  //ESP_LOGI(TAG,__func__);
	  app_hmi_widget_parsing_();
	  rsc_statemachine_state(&self_.statemachine);

      vTaskDelay(100 / portTICK_PERIOD_MS);

  }

}

/********************** widgets handlers functions definition *******************/

//Test
void widget_test_action_handler_( void){
	ESP_LOGI(TAG, "test widget ");
	api_stone_tx_set_text("nombre_wd_34", API_STONE_SET_TEXT_EDIT, "hola_hector", strlen("hola_hector"));
}

//Menu
void widget_menu_to_programs_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_programs_);
		//OPEN WINDOW
		api_stone_open_window("new_program");
	}
}

void widget_menu_to_manual_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_manual_);
		//OPEN WINDOW
		api_stone_open_window("manual");
	}
}

void widget_menu_to_new_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_new_program_);
		//OPEN WINDOW
		api_stone_open_window("new");
	}
}


void widget_menu_to_settings_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_settings_);
		//OPEN WINDOW
		api_stone_open_window("settings");
	}
}

void widget_menu_to_info_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_info_);
		//OPEN WINDOW
		api_stone_open_window("info");
	}
}

void widget_menu_to_running_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_running_program_);
		//OPEN WINDOW
		api_stone_open_window("running_program");
	}
}
//PROGRAMS
void widget_next_page_action_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_programs_vars_.next_page=true;
	}
}

void widget_prev_page_action_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_programs_vars_.previous_page=true;
	}
}

void widget_programs_to_menu_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
		//OPEN WINDOW
		api_stone_open_window("menu");
	}
}

void widget_slot1_selected_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_selected_program_);
		window_selected_program_vars_.triger_slotn=1;
		//OPEN WINDOW
		api_stone_open_window("selected_program");
	}
}

void widget_slot2_selected_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_selected_program_);
		window_selected_program_vars_.triger_slotn=2;
		//OPEN WINDOW
		api_stone_open_window("selected_program");
	}
}

void widget_slot3_selected_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_selected_program_);
		window_selected_program_vars_.triger_slotn=3;
		//OPEN WINDOW
		api_stone_open_window("selected_program");
	}
}

void widget_slot4_selected_handler_(void){

}

void widget_slot5_selected_handler_(void){

}

void widget_programs_to_new_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_new_program_);
		//OPEN WINDOW
		api_stone_open_window("new");
	}
}

//Selected
void widget_play_selected_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_selected_program_vars_.play=true;
	}
}


void widget_delete_selected_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_selected_program_vars_.delete=true;
	}
}

void widget_selected_to_menu_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
		//OPEN WINDOW
		api_stone_open_window("menu");
	}
}

//Running
void widget_running_to_menu_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
		//OPEN WINDOW
		api_stone_open_window("menu");
	}
}

//New
void widget_new_to_menu_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
		//OPEN WINDOW
		api_stone_open_window("menu");
	}
}

void widget_new_play_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_new_program_vars_.play=true;
	}
}

void widget_new_save_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_new_program_vars_.save=true;
	}
}

void widget_new_name_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s: name %s",__func__,self_.wd_text);
		strcpy(window_new_program_vars_.program.name,self_.wd_text);
	}
}

void widget_new_var1_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s: var1 %d",__func__,self_.wd_int);
		window_new_program_vars_.program.v1=self_.wd_int;
	}
}

//Manual
void widget_manual_to_menu_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
		//OPEN WINDOW
		api_stone_open_window("menu");
	}
}

//Settings

void widget_settings_to_wifi_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_wifi_);
		//OPEN WINDOW
		api_stone_open_window("wifi");
	}
}


void widget_settings_to_menu_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
		//OPEN WINDOW
		api_stone_open_window("menu");
	}
}

void widget_settings_reset_m_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		app_hmi_reset_machine();
	}
}

//Info
void widget_info_to_menu_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
		//OPEN WINDOW
		api_stone_open_window("menu");
	}
}


//Wifi
void widget_wifi_to_settings_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_settings_);
		//OPEN WINDOW
		api_stone_open_window("settings");
	}
}

void widget_wifi_s1_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_wifi_connect_);
		window_wifi_connect_vars_.triger_slotn=1;
		//OPEN WINDOW
		api_stone_open_window("wifi_connect");
	}
}

void widget_wifi_s2_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_wifi_connect_);
		window_wifi_connect_vars_.triger_slotn=2;
		//OPEN WINDOW
		api_stone_open_window("wifi_connect");
	}
}

void widget_wifi_s3_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_wifi_connect_);
		window_wifi_connect_vars_.triger_slotn=3;
		//OPEN WINDOW
		api_stone_open_window("wifi_connect");
	}
}

void widget_wifi_s4_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_wifi_connect_);
		window_wifi_connect_vars_.triger_slotn=4;
		//OPEN WINDOW
		api_stone_open_window("wifi_connect");
	}
}

void widget_wifi_next_page_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_wifi_vars_.next_page=true;
	}
}

void widget_wifi_prev_page_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_wifi_vars_.previous_page=true;
	}
}


void widget_wifi_disconnect_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		api_wifi_sta_disconnect();
		//OPEN WINDOW
		api_stone_open_window("menu");
	}
}

//Wifi connect
void widget_wificonnect_ok_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_wifi_connect_vars_.ok=true;
	}
}

void widget_wificonnect_to_menu_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
		//OPEN WINDOW
		api_stone_open_window("menu");
	}
}

void widget_wificonnect_password_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s: name %s",__func__,self_.wd_text);
		strcpy(window_wifi_connect_vars_.password,self_.wd_text);
	}
}

/********************** states handlers functions definition *******************/

static void state_window_menu_(rsc_statemachine_t *sm){
	ESP_LOGI(TAG,"%s",__func__);
    //Entry
    if (rsc_statemachine_is_entry(sm))
    {

    }
    //Loop

    //Exit
    if (rsc_statemachine_is_exit(sm))
    {

    }
}
static void state_window_manual_(rsc_statemachine_t *sm){
	ESP_LOGI(TAG,"%s",__func__);
    //Entry
    if (rsc_statemachine_is_entry(sm))
    {

    }
    //Loop

    //Exit
    if (rsc_statemachine_is_exit(sm))
    {

    }
}
static void state_window_settings_(rsc_statemachine_t *sm){
	ESP_LOGI(TAG,"%s",__func__);
    //Entry
    if (rsc_statemachine_is_entry(sm))
    {

    }
    //Loop

    //Exit
    if (rsc_statemachine_is_exit(sm))
    {

    }
}

static void state_window_info_(rsc_statemachine_t *sm){
	ESP_LOGI(TAG,"%s",__func__);
	static uint8_t mac[6];
    //Entry
    if (rsc_statemachine_is_entry(sm))
    {
    	board_mac_get_default(mac);
    	api_stone_tx_set_int("info_mac", API_STONE_SET_TEXT_LABEL, mac[0]);

    }
    //Loop



    //Exit
    if (rsc_statemachine_is_exit(sm))
    {

    }
}

static void state_window_programs_(rsc_statemachine_t* sm)
{
	ESP_LOGI(TAG,"%s",__func__);

	static int32_t programs_in_mem = 0;
	static int32_t n_pages = 0;
	static api_program_manager_program_t aux_program;

    //Entry
    if (rsc_statemachine_is_entry(sm))
    {
    	//init variables
    	window_programs_vars_.actual_page = 1;
    	window_programs_vars_.next_page = false;
    	window_programs_vars_.previous_page = false;

    	//interface with api programs
    	programs_in_mem = api_program_manager_get_n_programs_in_mem();
    	ESP_LOGI(TAG,"programs_in_mem: %d", programs_in_mem);
    	if(programs_in_mem <= APP_HMI_N_PROGRAMS_PER_PAGE){
    		n_pages = 1;
    	}
    	else{
    		n_pages = (int32_t)ceil((double)programs_in_mem/APP_HMI_N_PROGRAMS_PER_PAGE);
    	}
        	ESP_LOGI(TAG,"n_pages: %d", n_pages);


    	for (int i = 1; i <= APP_HMI_N_PROGRAMS_PER_PAGE; i++) {
    		if (true == api_program_manager_read(&aux_program, i)) {
    			ESP_LOGI(TAG,"%s",aux_program.name);
    			app_hmi_write_slot_name_(i,aux_program.name, strlen(aux_program.name));
    			ESP_LOGI(TAG,"READED SLOT %d: %s", i,  aux_program.name);
    		}
    		else{
    			app_hmi_write_slot_name_(i,"empty", strlen("empty"));
    			ESP_LOGI(TAG,"EMPTY SLOT %d", i);
    		}
    	}


     }
    // Loop

        if (true == window_programs_vars_.next_page){

        	window_programs_vars_.actual_page++;
        	ESP_LOGI(TAG,"actual_page: %d",window_programs_vars_.actual_page);

        	if(window_programs_vars_.actual_page <= n_pages ){
        		int k =1;
            	for (int i = (window_programs_vars_.actual_page-1)*APP_HMI_N_PROGRAMS_PER_PAGE+1;
            			i <= window_programs_vars_.actual_page*APP_HMI_N_PROGRAMS_PER_PAGE; i++) {

            		if (true == api_program_manager_read(&aux_program, i)) {
            			app_hmi_write_slot_name_(k,aux_program.name, strlen(aux_program.name));
            			ESP_LOGI(TAG,"READED SLOT %d: %s", i,  aux_program.name);
            		}
            		else{
            			app_hmi_write_slot_name_(k,"empty", strlen("empty"));
            			ESP_LOGI(TAG,"EMPTY SLOT %d", i);
            		}
            		k++;
            	}
        	}
        	else{
            	window_programs_vars_.actual_page = n_pages;
        		ESP_LOGI(TAG,"Last page");
        	}

            window_programs_vars_.next_page=false;
        }

       if (true == window_programs_vars_.previous_page){

    	   window_programs_vars_.actual_page--;

       	if(window_programs_vars_.actual_page  >= 1 ){
       		int k =1;
        	for (int i = (window_programs_vars_.actual_page-1)*APP_HMI_N_PROGRAMS_PER_PAGE+1;
        			i <= window_programs_vars_.actual_page*APP_HMI_N_PROGRAMS_PER_PAGE; i++) {
        		if (true == api_program_manager_read(&aux_program, i)) {
        			app_hmi_write_slot_name_(i,aux_program.name, strlen(aux_program.name));
        			ESP_LOGI(TAG,"READED SLOT %d: %s", i,  aux_program.name);
        		}
        		else{
        			app_hmi_write_slot_name_(i,"empty", strlen("empty"));
        			ESP_LOGI(TAG,"EMPTY SLOT %d", i);
        		}
        		k++;
        	}
       	}
       	else{
       		window_programs_vars_.actual_page = 1;
       		ESP_LOGI(TAG,"First page");
       	}
            window_programs_vars_.previous_page=false;
        }

    //Exit
    if (rsc_statemachine_is_exit(sm))
    {
    }
}

static void state_window_selected_program_(rsc_statemachine_t *sm){
	ESP_LOGI(TAG,"%s",__func__);

	static api_program_manager_program_t aux_program;
    //Entry
    if (rsc_statemachine_is_entry(sm))
    {
    	window_selected_program_vars_.selected=0;
    	window_selected_program_vars_.play=false;
    	window_selected_program_vars_.delete=false;
    }

    if(0!= window_selected_program_vars_.triger_slotn){

    	window_selected_program_vars_.selected= window_selected_program_vars_.triger_slotn + ((window_programs_vars_.actual_page-1) * APP_HMI_N_PROGRAMS_PER_PAGE);

		if (true == api_program_manager_read(&aux_program, window_selected_program_vars_.selected)) {
			app_hmi_write_selected_program_(aux_program);
			ESP_LOGI(TAG,"READED SLOT %d: %s", window_selected_program_vars_.selected,  aux_program.name);
		}
		else{
			ESP_LOGI(TAG,"EMPTY SLOT %d",window_selected_program_vars_.selected);
			rsc_statemachine_go_to(sm, state_window_programs_);
		}

		window_selected_program_vars_.triger_slotn=0;
    }

    if(0!= window_selected_program_vars_.selected){

    	//PLAY or DELETE
    	if(true == window_selected_program_vars_.play){

    		app_hmi_run_program(aux_program);

    		rsc_statemachine_go_to(sm, state_window_running_program_);
    	}
    	else if(true == window_selected_program_vars_.delete){

    		app_hmi_delete_program(window_selected_program_vars_.selected);

    		rsc_statemachine_go_to(sm, state_window_menu_);
    	}

    }
    //Exit
    if (rsc_statemachine_is_exit(sm))
    {
    	window_selected_program_vars_.triger_slotn=0;
    }
}

static void state_window_new_program_(rsc_statemachine_t *sm){

	ESP_LOGI(TAG,"%s",__func__);


	static bool first_entry= true;

    //Entry
    if (rsc_statemachine_is_entry(sm))
    {
    	window_new_program_vars_.play=false;
    	window_new_program_vars_.save=false;

    	if(true == first_entry){
        	strcpy(window_new_program_vars_.program.name,"default_name");
        	window_new_program_vars_.program.v1=0; //VAR1 DEFAULT VALUE

        	api_stone_tx_set_text("new_name", API_STONE_SET_TEXT_LABEL, window_new_program_vars_.program.name, strlen(window_new_program_vars_.program.name));
        	api_stone_tx_set_int("new_var1", API_STONE_SET_TEXT_LABEL, window_new_program_vars_.program.v1);

        	first_entry = false;
    	}

    }

	//PLAY or DELETE
	if(true == window_new_program_vars_.play){

		app_hmi_run_program(window_new_program_vars_.program);

		rsc_statemachine_go_to(sm, state_window_running_program_);
	}
	else if(true == window_new_program_vars_.save){

		app_hmi_save_program(&window_new_program_vars_.program);

		rsc_statemachine_go_to(sm, state_window_menu_);
	}

    //Exit
    if (rsc_statemachine_is_exit(sm))
    {

    }
}

static void state_window_running_program_(rsc_statemachine_t *sm){
	ESP_LOGI(TAG,"%s",__func__);

	static uint32_t start_time=0;
	static uint32_t actual_time=0;

    //Entry
    if (rsc_statemachine_is_entry(sm))
    {

    	//START TIMERS
    	start_time = board_timer_get_time_ms();
    	actual_time = board_timer_get_time_ms();

    }

    //SEND REAL TIME DATA
	if ( 2000 < (actual_time - start_time)  ){

		  api_stone_tx_set_int("running_time", API_STONE_SET_TEXT_LABEL, board_timer_get_time_ms());
		  api_stone_tx_set_int("running_varx", API_STONE_SET_TEXT_LABEL, window_running_program_vars_.varx);
		  api_stone_tx_set_int("running_vary", API_STONE_SET_TEXT_LABEL,  window_running_program_vars_.vary);

		  window_running_program_vars_.varx++;
		  window_running_program_vars_.vary++;
		  start_time = board_timer_get_time_ms();
	}

    //UPDATE ACTUAL TIME
    actual_time = board_timer_get_time_ms();

    //Exit
    if (rsc_statemachine_is_exit(sm))
    {

    }

}


static void state_window_wifi_(rsc_statemachine_t *sm){

		ESP_LOGI(TAG,"%s",__func__);

		static int32_t n_pages = 0;
		static api_wifi_record_t aux_wifi_record;

	    //Entry
	    if (rsc_statemachine_is_entry(sm))
	    {

	    	//scan
	    	api_wifi_scan();

	    	//init variables
	    	window_wifi_vars_.actual_page = 1;
	    	window_wifi_vars_.next_page = false;
	    	window_wifi_vars_.previous_page = false;

	    	uint16_t scaned_wifi_num = api_wifi_scan_get_n();
	    	ESP_LOGI(TAG,"n of scanned wifi ap: %d", scaned_wifi_num);

	    	if(scaned_wifi_num <= APP_HMI_N_WIFI_PER_PAGE){
	    		n_pages = 1;
	    	}
	    	else{
	    		n_pages = (int32_t)ceil((double)scaned_wifi_num/APP_HMI_N_WIFI_PER_PAGE);
	    	}
	        	ESP_LOGI(TAG,"n_pages: %d", n_pages);


	    	for (int i = 1; i <= APP_HMI_N_WIFI_PER_PAGE; i++) {

	    		if( true ==  api_wifi_scan_get_wifi_record(i, &aux_wifi_record)){
	    			app_hmi_write_wifi_slot_name_(i,(char*)aux_wifi_record.ssid, strlen( (char*)aux_wifi_record.ssid) );
	    			ESP_LOGI(TAG,"SCANED WIFI %d: %s", i ,(char*)aux_wifi_record.ssid);
	    		}
	    		else{
	    			app_hmi_write_wifi_slot_name_(i,"empty", strlen("empty"));
	    			ESP_LOGI(TAG,"EMPTY SLOT %d", i);
	    		}
	    	}
	     }
	    // Loop

	        if (true == window_wifi_vars_.next_page){

	        	window_wifi_vars_.actual_page++;
	        	ESP_LOGI(TAG,"actual_page: %d",window_wifi_vars_.actual_page);

	        	if(window_wifi_vars_.actual_page <= n_pages ){
	        		int k =1;
	            	for (int i = (window_wifi_vars_.actual_page-1)*APP_HMI_N_WIFI_PER_PAGE+1;
	            			i <= window_wifi_vars_.actual_page*APP_HMI_N_WIFI_PER_PAGE; i++) {
	            		if (true == api_wifi_scan_get_wifi_record(i,&aux_wifi_record)) {
	    	    			app_hmi_write_wifi_slot_name_(k,(char*)aux_wifi_record.ssid, strlen( (char*)aux_wifi_record.ssid) );
	            			ESP_LOGI(TAG,"READED SLOT %d: %s", i,  (char*)aux_wifi_record.ssid);
	            		}
	            		else{
	            			app_hmi_write_slot_name_(k,"empty", strlen("empty"));
	            			ESP_LOGI(TAG,"EMPTY SLOT %d", i);
	            		}
	            		k++;
	            	}
	        	}
	        	else{
	        		window_wifi_vars_.actual_page = n_pages;
	        		ESP_LOGI(TAG,"Last page");
	        	}

	        	window_wifi_vars_.next_page=false;
	        }

	       if (true == window_wifi_vars_.previous_page){

	    	   window_wifi_vars_.actual_page--;

	       	if(window_wifi_vars_.actual_page  >= 1 ){
	       		int k =1;
	        	for (int i = (window_programs_vars_.actual_page-1)*APP_HMI_N_PROGRAMS_PER_PAGE+1;
	        			i <= window_programs_vars_.actual_page*APP_HMI_N_PROGRAMS_PER_PAGE; i++) {
	        		if (true == api_wifi_scan_get_wifi_record(i,&aux_wifi_record)) {
    	    			app_hmi_write_wifi_slot_name_(i,(char*)aux_wifi_record.ssid, strlen( (char*)aux_wifi_record.ssid) );
	        			ESP_LOGI(TAG,"READED SLOT %d: %s", i, (char*)aux_wifi_record.ssid);
	        		}
	        		else{
	        			app_hmi_write_slot_name_(i,"empty", strlen("empty"));
	        			ESP_LOGI(TAG,"EMPTY SLOT %d", i);
	        		}
	        		k++;
	        	}
	       	}
	       	else{
	       		window_wifi_vars_.actual_page = 1;
	       		ESP_LOGI(TAG,"First page");
	       	}
	       	window_wifi_vars_.previous_page=false;
	        }

	    //Exit
	    if (rsc_statemachine_is_exit(sm))
	    {
	    }

}
static void state_window_wifi_connect_(rsc_statemachine_t *sm){
	ESP_LOGI(TAG,"%s",__func__);

	static api_wifi_record_t aux_wifi_record;
	static bool try_to_connect;
	static uint32_t reconnect_counter;
	static uint32_t reconnect_timer;

    //Entry
    if (rsc_statemachine_is_entry(sm))
    {
    	//flags
    	window_wifi_connect_vars_.selected=0;
    	window_wifi_connect_vars_.ok=false;
    	try_to_connect=false;
    	reconnect_counter=0;
    	app_hmi_write_wifi_status_("Insert password.");

    	//set labels
    	app_hmi_write_wifi_status_("Insert password and press Ok");
    	api_stone_tx_set_text("wificonnect_pass", API_STONE_SET_TEXT_LABEL, " ", strlen(" "));
    	strcpy(window_wifi_connect_vars_.password," ");
    }

    if(0!= window_wifi_connect_vars_.triger_slotn){

    	window_wifi_connect_vars_.selected= window_wifi_connect_vars_.triger_slotn + ((window_wifi_vars_.actual_page-1) * APP_HMI_N_WIFI_PER_PAGE);

		if (true == api_wifi_scan_get_wifi_record( window_wifi_connect_vars_.selected,&aux_wifi_record)) {
			app_hmi_write_selected_wifi_(aux_wifi_record);
			ESP_LOGI(TAG,"SELECTED WIFI %d: %s", window_wifi_connect_vars_.selected, (char*) aux_wifi_record.ssid);
		}
		else{
			ESP_LOGI(TAG,"EMPTY SLOT %d",window_wifi_connect_vars_.selected);
			rsc_statemachine_go_to(sm, state_window_menu_);
		}

		window_wifi_connect_vars_.triger_slotn=0;
    }


    if(0!= window_wifi_connect_vars_.selected){

    	//MAKE CONNECTION
    	if(true == window_wifi_connect_vars_.ok){

    		ESP_LOGI(TAG,"SSID: %s", (char*) aux_wifi_record.ssid);
    		ESP_LOGI(TAG,"PASS: %s", window_wifi_connect_vars_.password);

    		api_wifi_sta_disconnect();
    		api_wifi_sta_connect((char*) aux_wifi_record.ssid , window_wifi_connect_vars_.password);
    		ESP_LOGI(TAG,"Connecting. Please wait....");
    		app_hmi_write_wifi_status_("Connecting.");
    		try_to_connect = true;
    		window_wifi_connect_vars_.ok=false;
    		reconnect_counter=0;
    		reconnect_timer=board_timer_get_time_ms();
    	}
    }

    if(1000 < (board_timer_get_time_ms() - reconnect_timer)){
        if(true== try_to_connect){
        	if(true == api_wifi_sta_is_connected()){
        		ESP_LOGI(TAG,"Connected!!!");
        		app_hmi_write_wifi_status_("Connected!");
        		try_to_connect = false;
        	}
        	else{
        		 api_wifi_sta_reconnect();
        		 reconnect_counter++;
        	}
        }
        if(reconnect_counter > 5){
        	reconnect_counter=0;
        	try_to_connect=false;
    		ESP_LOGI(TAG,"Error connecting.");
        	app_hmi_write_wifi_status_("Error connecting.");
        	api_wifi_sta_disconnect();
        }
        reconnect_timer = board_timer_get_time_ms();
    }

    //Exit
    if (rsc_statemachine_is_exit(sm))
    {
    	window_selected_program_vars_.triger_slotn=0;
    }
}
/********************** end of file ******************************************/
/** @}Final Doxygen */





