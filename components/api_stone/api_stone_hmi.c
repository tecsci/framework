/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		api_stone_hmi.c
* @date		8 oct. 2021
* @author	
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/******** inclusions ***************/

#include "api_stone_hmi.h"
#include "api_serial.h"
#include "rsc_json_lib.h"
#include "esp_log.h"

/******** macros and definitions ***********/

#define API_STONE_TX_HEADER "ST<"
#define API_STONE_TX_TAIL ">ET"

#define API_STONE_INT_TO_ARRAY_SIZE 8

/******** internal data declaration **********/

/******** internal data definition ***********/

static const char*TAG ="api_stone";

static struct{
	char send_buffer[API_STONE_TX_MAX_DATA_LEN];
}self_;

/******** external data definition ***********/

/******** internal functions definition ********/

/******** external functions definition ********/


bool api_stone_rx_process_button(char* name, api_stone_rx_button_val_t *p_button, uint8_t * data, uint32_t data_len){

	int i =0;

	//NAME
	for(i=0 ;i < (data_len-1) ; i++){
		name[i]= (char)data[i];
	}
	name[data_len-1] = '\0';
	//ESP_LOGI(TAG, "command widget name: %s", name);

	//COMMAND DATA
	if((API_STONE_BUTTON_PRESSED != data[data_len-1]) &&
			(API_STONE_BUTTON_RELEASE != data[data_len-1]) &&
			(API_STONE_BUTTON_LONG != data[data_len-1])
			){
		return false;
	}

	*p_button = (api_stone_rx_button_val_t) data[data_len-1];

	return true;
}

bool api_stone_rx_process_edit_text(char* name, char* text, uint8_t * data, uint32_t data_len){

	int i,j =0;
	bool end_chr_founded= false;

	//NAME
	if( '\"' != data[0]){
		return false;
	}

	for(i=1 ; i < data_len ; i++){
		if( '\"' == data[i]){
			end_chr_founded=true;
			break;
		}
		name[i-1]= (char)data[i];
	}

	if(false == end_chr_founded){
		return false;
	}

	name[i-1] = '\0';

	//DATA TEXT
	for(j=i+2 ; j< data_len ; j++){
		text[j-2-i]= (char)data[j];
	}

	text[j-2-i] = '\0';

	return true;
}

bool api_stone_rx_process_edit_int(char* name, int32_t* p_int, uint8_t * data, uint32_t data_len){

	int i =0;
	//NAME
	for(i=0 ;i < (data_len-4) ; i++){
		name[i]= (char)data[i];
	}
	name[data_len-4] = '\0';

	//COMMAND DATA

	*p_int = (data[data_len-4] << 24) | (data[data_len-3] << 16)
			 | (data[data_len-2] << 8) | (data[data_len-1]);

	return true;
}

bool api_stone_tx_set_enabled(char * str_widget_name, bool enable ){

	//ST<{"cmd_code":"set_enable","type":"widget","widget":"button1","enable":true}>ET

	//variables

	rsc_json_lib_t *nuevo_json;
	rsc_json_lib_create(&nuevo_json);

	//agregar header
	strcpy(self_.send_buffer,API_STONE_TX_HEADER);

	//agregar adata al json obj
	if (false == rsc_json_lib_add_data(nuevo_json, "cmd_code" ,"set_enable")){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	if (false == rsc_json_lib_add_data(nuevo_json, "type" , "widget")){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	if (false == rsc_json_lib_add_data(nuevo_json, "widget" , str_widget_name)){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	if (false == rsc_json_lib_add_bool(nuevo_json, "enable" , enable)){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	//serializar --> obs: le pasamos desde donde queremos
	if( false == rsc_json_lib_serialize(nuevo_json,
			self_.send_buffer+strlen(self_.send_buffer),
			API_STONE_TX_MAX_DATA_LEN-strlen(self_.send_buffer))){
		ESP_LOGE(TAG, "error serializing");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	strcat(self_.send_buffer,API_STONE_TX_TAIL);

	api_serial_write(API_STONE_TX_UART_NUM, self_.send_buffer, strlen(self_.send_buffer));

	//delete json obj
	rsc_json_lib_delete(nuevo_json);
	return true;

}

bool api_stone_tx_set_visible(char * str_widget_name, bool visible ){

	//ST<{"cmd_code":"set_visible","type":"widget","widget":"button1","visible":true}>ET

	//variables

	rsc_json_lib_t *nuevo_json;
	rsc_json_lib_create(&nuevo_json);

	//agregar header
	strcpy(self_.send_buffer,API_STONE_TX_HEADER);


	//agregar adata al json obj
	if (false == rsc_json_lib_add_data(nuevo_json, "cmd_code" ,"set_visible")){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	if (false == rsc_json_lib_add_data(nuevo_json, "type" , "widget")){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	if (false == rsc_json_lib_add_data(nuevo_json, "widget" , str_widget_name)){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	if (false == rsc_json_lib_add_bool(nuevo_json, "visible" , visible)){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	//serializar --> obs: le pasamos desde donde queremos
	if( false == rsc_json_lib_serialize(nuevo_json,
			self_.send_buffer+strlen(self_.send_buffer),
			API_STONE_TX_MAX_DATA_LEN-strlen(self_.send_buffer))){
		ESP_LOGE(TAG, "error serializing");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	strcat(self_.send_buffer,API_STONE_TX_TAIL);

	api_serial_write(API_STONE_TX_UART_NUM, self_.send_buffer, strlen(self_.send_buffer));

	//delete json obj
	rsc_json_lib_delete(nuevo_json);
	return true;

}

 bool api_stone_tx_set_text(char * str_widget_name, api_stone_tx_set_text_widget_type_t type, char* str_data, uint32_t data_len ){

	// del codigo de Nigel --> "ST<{\"cmd_code\":\"set_image\",\"type\":\"image\",\"widget\":\"image35\",\"image\":light_green}>ET"

	if(data_len > API_STONE_TX_MAX_DATA_LEN-20){
		return false;
	}

	//variables
	rsc_json_lib_t *nuevo_json;
	rsc_json_lib_create(&nuevo_json);


	//agregar header
	strcpy(self_.send_buffer,API_STONE_TX_HEADER);

	//agregar adata al json obj
	if (false == rsc_json_lib_add_data(nuevo_json, "cmd_code" ,"set_text")){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}


	bool aux_rst= true;
	switch(type){

		case API_STONE_SET_TEXT_EDIT:
			aux_rst = rsc_json_lib_add_data(nuevo_json, "type" ,"edit");
			break;
		case API_STONE_SET_TEXT_LABEL:
			aux_rst = rsc_json_lib_add_data(nuevo_json, "type" ,"label");
			break;
		case API_STONE_SET_TEXT_SELECTOR:
			aux_rst = rsc_json_lib_add_data(nuevo_json, "type" ,"text_selector");
			break;
		default:
			ESP_LOGE(TAG, "error adding");
			rsc_json_lib_delete(nuevo_json);
			return false;
	}

	if(false == aux_rst){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	if (false == rsc_json_lib_add_data(nuevo_json, "widget" ,str_widget_name)){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	if (false == rsc_json_lib_add_data(nuevo_json, "text" , str_data)){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	//serializar --> obs: le pasamos desde donde queremos
	if( false == rsc_json_lib_serialize(nuevo_json,
			self_.send_buffer+strlen(self_.send_buffer),
			API_STONE_TX_MAX_DATA_LEN-strlen(self_.send_buffer))){
		ESP_LOGE(TAG, "error serializing");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	strcat(self_.send_buffer,API_STONE_TX_TAIL);

	api_serial_write(API_STONE_TX_UART_NUM, self_.send_buffer, strlen(self_.send_buffer));

	//delete json obj
	rsc_json_lib_delete(nuevo_json);
	return true;


}

 bool api_stone_tx_set_int(char * str_widget_name, api_stone_tx_set_text_widget_type_t type,  int32_t data ){

	 	char aux_itoa[API_STONE_INT_TO_ARRAY_SIZE];
		itoa(data,aux_itoa,10);
		if(false == api_stone_tx_set_text(str_widget_name, type, aux_itoa, strlen(aux_itoa))){
			return false;
		}

		return false;
 }

bool api_stone_open_window(char * str_window_name ){
	// ST<{"cmd_code":"open_win","widget":"label_value"}>ET

	//variables
	rsc_json_lib_t *nuevo_json;
	rsc_json_lib_create(&nuevo_json);

	//agregar header
	strcpy(self_.send_buffer,API_STONE_TX_HEADER);

	//agregar adata al json obj
	if (false == rsc_json_lib_add_data(nuevo_json, "cmd_code" ,"open_win")){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	if (false == rsc_json_lib_add_data(nuevo_json, "widget" ,str_window_name)){
		ESP_LOGE(TAG, "error adding");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	//serializar --> obs: le pasamos desde donde queremos
	if( false == rsc_json_lib_serialize(nuevo_json,
			self_.send_buffer+strlen(self_.send_buffer),
			API_STONE_TX_MAX_DATA_LEN-strlen(self_.send_buffer))){
		ESP_LOGE(TAG, "error serializing");
		rsc_json_lib_delete(nuevo_json);
		return false;
	}

	strcat(self_.send_buffer,API_STONE_TX_TAIL);

	api_serial_write(API_STONE_TX_UART_NUM, self_.send_buffer, strlen(self_.send_buffer));

	//delete json obj
	rsc_json_lib_delete(nuevo_json);
	return true;

}

void api_stone_hmi_init(){

}
void api_stone_hmi_deinit(){

}
void api_stone_hmi_loop(){

}

/******** end of file **************/

/** @}Final Doxygen */
