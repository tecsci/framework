/* 
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		api_bme280.h
* @date		23 sep. 2021
* @author	
*		-Martin Abel Gambarotta   <magambarotta@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef COMPONENTS_API_BOSCH_INCLUDE_API_BME280_H_
#define COMPONENTS_API_BOSCH_INCLUDE_API_BME280_H_
/********************** inclusions *******************************************/

#include "bme280.h"
#include "bme280_defs.h"

/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

double api_bosch_bme280_get_temperature(void);
double api_bosch_bme280_get_pressure(void);
double api_bosch_bme280_get_humidity(void);

void api_bosch_bme280_init();
void api_bosch_bme280_deinit();
void api_bosch_bme280_loop();

#endif /* COMPONENTS_API_BOSCH_INCLUDE_API_BME280_H_ */
/** @}Final Doxygen */
