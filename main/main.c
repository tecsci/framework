/*
 * <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
 * 			Copyright (C) <2021>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file		main.c
 * @date		15 jun. 2021
 * @author
 *		-Martin Abel Gambarotta   (magambarotta@gmail.com)
 * @version	v1.0.0
 *
 * @brief
 * @{ Init Doxygen
 */

/********************** inclusions *******************************************/

#include "main.h"



#include "mod_hmi_RX.h"
#include "app_hmi.h"
#include "app_test.h"


#include "board.h"
#include "api.h"
#include "api_stone_hmi.h"

#if 0
//Test Reset Timer
#include "esp_timer.h"
#include "xtensa/core-macros.h"
#include "esp_log.h"
#endif

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

void app_main(void) {

	//Init Framework
	board_init();
	api_init();
	//api_stone_hmi_init();

	//Init Task
	app_test_init();
	//app_hmi_init();
	//mod_hmi_RX_init();

	for (;;) {

#if 0
    	ESP_LOGI("app_principal\r\n");
    	uint32_t time2=esp_log_timestamp();
    	ESP_LOGI("app_principal:", "Time2 -> %u",time2); 			//Tiempo en milis desde inicio
#endif

		board_loop();
		api_loop();

		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
}

/********************** external functions definition ************************/

/********************** end of file ******************************************/
/** @}Final Doxygen */

